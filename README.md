# Dokumentacja Skryptu Python do Wyszukiwania Adresów Email

Ten skrypt Python został stworzony do ekstrakcji adresów email z pliku tekstowego. Skrypt czyta plik tekstowy linia po linii, przeszukując każde słowo pod kątem cech charakterystycznych dla adresów email.

## Jak używać

1. Przygotuj plik tekstowy zawierający tekst, z którego chcesz ekstrahować adresy email. Ten skrypt został przetestowany na pliku tekstowym `tekst.txt`.

2. Uruchom skrypt Python.

## Opis Kodu

Skrypt zaczyna się od importowania wymaganych bibliotek, tj. `numpy` i `re`. Następnie tworzy pustą listę `valid_emails`, do której będą dodawane poprawne adresy email.

Skrypt czyta plik `tekst.txt` linia po linii. Każdą linię dzieli na słowa za pomocą wyrażeń regularnych. Każde słowo jest następnie analizowane pod kątem cech charakterystycznych dla adresów email.

Do identyfikacji potencjalnych adresów email skrypt wykorzystuje wyrażenia regularne w celu wyszukania cech takich jak: znak '@', kropka, dwukropek, oraz specyficzne dla adresów email konstrukcje, takie jak `@.*com` czy `@.+\.`.

Dodatkowo skrypt zawiera pewne zabezpieczenia przed fałszywymi pozytywami, np. wykluczanie przypadków, gdy mamy wiele znaków '@' bez cudzysłowu, lub sytuacji, gdy adres zawiera cudzysłów, ale nie zaczyna się od niego.

Po przeszukaniu całego pliku, skrypt drukuje wszystkie znalezione poprawne adresy email.

## Załączony Plik Tekstowy

Załączony plik `tekst.txt` jest przykładowym plikiem, na którym można przetestować skrypt. Zawiera on tekst lorem ipsum z umieszczonymi w różnych miejscach i formatach adresami email. 

Ważne jest, aby pamiętać, że skrypt został zaprojektowany do obsługi określonego formatu tekstu i może nie działać poprawnie na tekstach o innych formatach.
