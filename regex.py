import numpy as np
import re

valid_emails = []


with open("tekst.txt", "r") as filestream:
    for line in filestream:
        currentline = re.split("\s", line)
        
        if(len(currentline) > 0 ):
        
            for word in currentline:
                at = re.findall("@", word)
                dot = re.findall("\.", word)
                bracket = re.findall("\:", word)
                coma = re.search(".+,$", word)
                paren1 = re.search("^\(.", word)
                paren2 = re.search(".+\)$", word)
                com = re.search("@.*com", word)
                atdot = re.search("@.+\.", word)
                if coma:
                    word = word[:-1]
                if paren1:
                    word = word[1:]
                if paren2:
                    word = word[:-1]
                quote = re.findall(chr(34), word)
                start_quote = re.search(chr(34), word)
                search_at = re.search("@", word)
                search_dot = re.search("\.", word)
                search_atdot = re.search("@\.", word)
                ipv6 = re.search("IPv6", word)
                # Jeżeli w adresie jest małpa
                if (len(at) > 0):
                    # ... i nie ma dokładnie jednego znaku cudzysłowia
                    if( len(quote) != 1):
                        # ... i jest przynajmniej jedna kropka lub 3 dwukropki
                        if((len(dot) > 0) or (len(bracket) > 2)):
                            # ... i nie występuje sytuacja gdzie jest kilka małp, a nie ma cudzysłowia
                            if not( (len(at) > 1) and (len(quote) == 0) ):
                                # ... i nie ma sytuacji gdzie adres zawiera cudzysłów, ale się od niego nie zaczyna
                                if not ( (len(quote) > 0) and (start_quote.start() != 0) ):
                                    # ... i po małpie nie ma od razu kropki
                                    if (search_atdot == None):
                                            # To już prawie na pewno jest to email :)

                                            # Który albo zawiera w sobie adres ipv6
                                            if (ipv6 != None):
                                                valid_emails.append(word)
                                            # Albo po małpie występuje kropka (ale nie odrazu)
                                            elif (atdot):
                                                valid_emails.append(word)
                                            # Albo po małpie występuje domena com
                                            elif (com):
                                                valid_emails.append(word)


for email in valid_emails:
    print(email)
                    




            
